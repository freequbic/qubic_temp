#!/usr/bin/ipython

import serial
import sys
import time
import os
import struct
from shutil import copyfile
import numpy as np
from scipy import interpolate

#datadir = os.path.expanduser('~/data/temperature/data/')
datadir = os.getcwd() + "/data/"

interface = "/dev/ttyUSB0"
baudrate = 19200
ser=serial.Serial(interface, baudrate, timeout=10)


data_length = 0
while (data_length == 0):
	string0 = ser.readline()
	data_length = len(string0)




timestring = "%04d%02d%02d_%02d%02d%02d" % (time.localtime()[0],time.localtime()[1], time.localtime()[2], time.localtime()[3], time.localtime()[4], time.localtime()[5])
dir = datadir + "log_cryo/dirfile_cryo_"+timestring+'/'

os.makedirs(dir)
copyfile(datadir+'log_cryo/format', dir+'format')

print "Logging cryo dirfiles to ", dir, "..."

command_cleanlink = "rm -f "+datadir+"log_cryo/dirfile_cryo_current"
os.system(command_cleanlink)
command_linkfile = "ln -f -s " + dir + " "+ datadir + "log_cryo/dirfile_cryo_current"
os.system(command_linkfile)

nchannel = 21
nT = 18

gain=[0.0626158803,0.0626095353,0.0626137451,0.0626129561,0.0626046339,0.0626023651,0.0626006617,0.0626026485,0.0625785803,0.0625785839,0.0625793290,0.0625781058,0.0625662881,0.0625695137,0.0625704064,0.0625677486,0.0626280689,0.0626252146,0.0626282954,0.0626263287]

offset=[-2049.6181153598,-2049.9601678205,-2049.2013364949,-2049.6533307630,-2049.2122546909,-2049.2859049597,-2049.5080966974,-2049.0869777510,-2048.5491308714,-2048.5516331324,-2048.5804165382,-2048.4528778901,-2048.5684350553,-2048.4054581436,-2048.3698491680,-2048.3218114873,-2050.3137988660,-2050.2215204946,-2050.2695627877,-2050.0539409956]

calibration_files = map(lambda x: datadir+"log_cryo/calibration/calibration_ch"+str(x).zfill(2)+".dat",range(nT))
calibration_tables = map(lambda x: np.loadtxt(x),calibration_files)

nfo = map(lambda x: dir+"cryo_ch"+str(x).zfill(2), range(nchannel))
fo = map(lambda x: open(x, "wb"), nfo)
ftime = open(dir+'CTIME_CRYO', 'wb')
ffram = open(dir+'FRAMENUM_CRYO', 'wb')
framenumber = 0

rawData = []
voltageData = []
temperatureData = []

#### Thermometers definitions ####
# T01 -> ch00 -> 40K filters
# T02 -> ch05 -> 


textfile = open(dir+'temperatures.dat', 'w')
textfile.write("#Time T01_40K_filters T09_shield_40K_down T05_PT1_stage_1 T06_PT2_stage_1 T12_shield_40K_right T02_4K_filters T03_HWP1 T04_HWP2 T10_shield_4K_down T11_shield_4K_right T13_PT1_stage_2 T14_PT2_stage_2 300mK-4CP-D-1 300mK-4HS-D-1 300mK-3CP-D-1 300mK-3HS-D-1 1K-4HS-D-1 1K-4CP-D-1\n")

while True:
    data_length = 0
    while (data_length == 0): 
        rawData = []
        voltageData = []
        temperatureData = []

        string = ser.readline().replace('\x00', '')
        data_length = len(string)
        rawData = map(int,string.split())
        
        for i in range(nT):
            voltageData.append(rawData[i+1]*gain[i]+offset[i])
            x=calibration_tables[i][:,0]
            y=calibration_tables[i][:,1] 
            interpolate_function = interpolate.interp1d(x,y,kind='linear',fill_value='extrapolate')
            temperatureData.append(interpolate_function(voltageData[i]))
            #print temperatureData[i]
        try:
            int(rawData[0])
        except ValueError:
            data_length = 0

    ctime = time.time()
    time.sleep(1)
    for i in range(nchannel-1):
        parola = struct.pack('<H',rawData[i+1]) # check endian
        fo[i].write(parola)
        #print i
    parola = struct.pack('<H',rawData[0] % 2**16)
    fo[nchannel-1].write(parola)
    
    textfile.write("%f\t" % ctime)
    for i in temperatureData:
        textfile.write("%f\t" % i)
    textfile.write("\n")
    textfile.flush()

    ffram.write(struct.pack('I', framenumber))
    ftime.write(struct.pack('d', ctime))
    ftime.flush()
    ffram.flush()       
    for i in range(nchannel):
        fo[i].flush()
    framenumber = framenumber+1
    #sys.stdout.write(str(framenumber) + '\n')
ser.close()

